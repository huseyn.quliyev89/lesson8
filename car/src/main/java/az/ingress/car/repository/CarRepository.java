package az.ingress.car.repository;

import az.ingress.car.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
    List<Car> findByModel(String model);

    @Query(
            nativeQuery = true,
            value
                    = "select * from car a where a.price=:carPrice")
    List<Car> findByPrice(@Param("carPrice") BigDecimal carPrice);

    @Query(nativeQuery = true, value="select count(*) from car")
    Integer countCars();
}
